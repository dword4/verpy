#!/usr/bin/env python3

import git
import semver

def check_files():
    my_repo = git.Repo('.')

    if my_repo.is_dirty(untracked_files=False):
        retv = 1
    else:
        retv = 0
    return retv
def main():
    return check_files()
if __name__ == '__main__':
    exit(main())
